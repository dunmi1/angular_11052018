# Welcome to Angular Class!

## Instructor

Eric Greene - [http://t4d.io](http://t4d.io)

## Schedule

Class:

- Monday through Friday: 9am to 5pm

Breaks:

- Morning Break: 10:25am to 10:35am
- Lunch: 12pm to 1pm
- Afternoon Break #1: 2:15pm to 2:25pm
- Afternoon Break #2: 3:40pm to 3:50pm

## Course Outline

- Day 1 - Angular CLI, Overview of Angular, Modules, Components, Composition
- Day 2 - Composition (cont'd), Pipes, Reactive Forms
- Day 3 - Services, Dependency Injection, Asynchronous Programming
- Day 4 - HttpClient & REST Services, RxJS, ngRx
- Day 5 - ngRx (cont'd), Routing, Unit Testing

## Links

### Instructor's Resources

- [Accelebrate, Inc.](https://www.accelebrate.com/)
- [WintellectNOW](https://www.wintellectnow.com/Home/Instructor?instructorId=EricGreene) - Special Offer Code: GREENE-2016
- [Microsoft Virtual Academy](https://mva.microsoft.com/search/SearchResults.aspx#!q=Eric%20Greene&lang=1033)

### Other Resources

- [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS)
- [JavaScript Air Podcast](http://javascriptair.podbean.com/)
- [Speaking JavaScript](http://speakingjs.com/es5/)

## Useful Resources

- [Angular CLI](https://cli.angular.io/)
- [TypeScript Coding Guidelines](https://github.com/Microsoft/TypeScript/wiki/Coding-guidelines)
- [Angular Style Guide](https://angular.io/docs/ts/latest/guide/style-guide.html)
- [Angular Cheat Sheet](https://angular.io/docs/ts/latest/guide/cheatsheet.html)
- [Angular API](https://angular.io/docs/ts/latest/api/)
