'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var _angular_core = require('@angular/core');
var _angular_common = require('@angular/common');

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = Object.setPrototypeOf ||
    ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
    function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}





function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}



function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

var PageableTable = (function () {
    function PageableTable() {
    }
    Object.defineProperty(PageableTable.prototype, "allData", {
        get: function () {
            return this._allData || [];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PageableTable.prototype, "dataPage", {
        set: function (d) {
            this._pageOfData = d;
        },
        enumerable: true,
        configurable: true
    });
    return PageableTable;
}());

var DataTableComponent = (function (_super) {
    __extends(DataTableComponent, _super);
    function DataTableComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.config = { cols: [] };
        return _this;
    }
    DataTableComponent_1 = DataTableComponent;
    Object.defineProperty(DataTableComponent.prototype, "data", {
        get: function () {
            return this._pageOfData;
        },
        set: function (d) {
            this._allData = d;
            this._pageOfData = d;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        _angular_core.Input(),
        __metadata("design:type", Object)
    ], DataTableComponent.prototype, "config", void 0);
    __decorate([
        _angular_core.Input(),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], DataTableComponent.prototype, "data", null);
    DataTableComponent = DataTableComponent_1 = __decorate([
        _angular_core.Component({
            selector: 'data-table',
            template: "<table>\n  <thead>\n    <tr>\n      <th *ngFor=\"let col of config.cols\">{{col.header}}</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let item of data\">\n      <td *ngFor=\"let col of config.cols\">\n        {{ item[col.field] | dataFormatPipe:col.format }}\n      </td>\n    </tr>\n  </tbody>\n</table>\n",
            styles: [""],
            providers: [
                { provide: PageableTable, useExisting: _angular_core.forwardRef(function () { return DataTableComponent_1; }) },
            ],
        })
    ], DataTableComponent);
    return DataTableComponent;
    var DataTableComponent_1;
}(PageableTable));

var FilterForm = (function () {
    function FilterForm() {
    }
    return FilterForm;
}());

var FilterTable = (function () {
    function FilterTable() {
        this._filteredData = [];
        this.currentPage = 0;
        this._data = [];
    }
    Object.defineProperty(FilterTable.prototype, "filteredData", {
        get: function () {
            return this._filteredData;
        },
        set: function (data) {
            if (this._filteredData && data && this._filteredData.length !== data.length) {
                this.currentPage = 0;
            }
            this._filteredData = data;
        },
        enumerable: true,
        configurable: true
    });
    return FilterTable;
}());

var FilteredTableComponent = (function () {
    function FilteredTableComponent() {
    }
    FilteredTableComponent.prototype.ngDoCheck = function () {
        this.filterTable.filteredData = this.filteredData;
    };
    Object.defineProperty(FilteredTableComponent.prototype, "filteredData", {
        get: function () {
            return !this.filterForm.filterValue
                ? this.filterTable.allData
                : this.filterTable.allData.filter(this.filterFn(this.filterForm.filterValue));
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        _angular_core.ContentChild(FilterForm),
        __metadata("design:type", FilterForm)
    ], FilteredTableComponent.prototype, "filterForm", void 0);
    __decorate([
        _angular_core.ContentChild(FilterTable),
        __metadata("design:type", FilterTable)
    ], FilteredTableComponent.prototype, "filterTable", void 0);
    __decorate([
        _angular_core.Input(),
        __metadata("design:type", Function)
    ], FilteredTableComponent.prototype, "filterFn", void 0);
    FilteredTableComponent = __decorate([
        _angular_core.Component({
            selector: 'filtered-table',
            template: "<ng-content select=\"[data-filter-form]\"></ng-content>\n<ng-content select=\"[data-filter-table]\"></ng-content>\n",
            styles: [""]
        })
    ], FilteredTableComponent);
    return FilteredTableComponent;
}());

var PaginatedTableComponent = (function (_super) {
    __extends(PaginatedTableComponent, _super);
    function PaginatedTableComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.pageLength = 10;
        _this.initialPage = 0;
        _this.listPropName = 'data';
        return _this;
    }
    PaginatedTableComponent_1 = PaginatedTableComponent;
    Object.defineProperty(PaginatedTableComponent.prototype, "data", {
        get: function () {
            return this._data;
        },
        set: function (d) {
            this._data = d || [];
            this.pageableTable.dataPage = this.listPage;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginatedTableComponent.prototype, "allData", {
        get: function () {
            return this.pageableTable.allData;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginatedTableComponent.prototype, "listPage", {
        get: function () {
            var startIndex = this.currentPage * this.pageLength;
            var endIndex = startIndex + this.pageLength;
            return (this.filteredData || this.data).slice(startIndex, endIndex);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginatedTableComponent.prototype, "totalPages", {
        get: function () {
            return Math.ceil((this.filteredData || this.data).length / this.pageLength);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginatedTableComponent.prototype, "firstPage", {
        get: function () {
            return this.currentPage <= 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginatedTableComponent.prototype, "lastPage", {
        get: function () {
            return this.currentPage + 1 >= this.totalPages;
        },
        enumerable: true,
        configurable: true
    });
    PaginatedTableComponent.prototype.ngOnInit = function () {
        this.currentPage = this.initialPage;
    };
    PaginatedTableComponent.prototype.ngAfterContentInit = function () {
        this.data = this.pageableTable.allData;
    };
    PaginatedTableComponent.prototype.ngAfterContentChecked = function () {
        if (this.data !== this.pageableTable.allData) {
            this.data = this.pageableTable.allData;
            this.filteredData = null;
            this.currentPage = this.initialPage;
        }
        this.pageableTable.dataPage = this.listPage;
    };
    PaginatedTableComponent.prototype.go = function (pages) {
        this.currentPage += pages;
        this.pageableTable.dataPage = this.listPage;
    };
    __decorate([
        _angular_core.Input(),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], PaginatedTableComponent.prototype, "data", null);
    __decorate([
        _angular_core.Input(),
        __metadata("design:type", Object)
    ], PaginatedTableComponent.prototype, "pageLength", void 0);
    __decorate([
        _angular_core.Input(),
        __metadata("design:type", Object)
    ], PaginatedTableComponent.prototype, "initialPage", void 0);
    __decorate([
        _angular_core.Input(),
        __metadata("design:type", Object)
    ], PaginatedTableComponent.prototype, "listPropName", void 0);
    __decorate([
        _angular_core.ContentChild(PageableTable),
        __metadata("design:type", PageableTable)
    ], PaginatedTableComponent.prototype, "pageableTable", void 0);
    PaginatedTableComponent = PaginatedTableComponent_1 = __decorate([
        _angular_core.Component({
            selector: 'paginated-table',
            template: "<ng-content></ng-content>\n<form>\n    <button type=\"button\" (click)=\"go(-1)\" [disabled]=\"firstPage\">Prev</button>\n    <button type=\"button\" (click)=\"go(1)\" [disabled]=\"lastPage\">Next</button>\n    <span>\n        Page {{currentPage+1}} of {{totalPages}}\n    </span>\n</form>\n",
            styles: [""],
            providers: [
                { provide: FilterTable, useExisting: _angular_core.forwardRef(function () { return PaginatedTableComponent_1; }) }
            ],
        })
    ], PaginatedTableComponent);
    return PaginatedTableComponent;
    var PaginatedTableComponent_1;
}(FilterTable));

var DataFormatPipePipe = (function () {
    function DataFormatPipePipe() {
    }
    DataFormatPipePipe.prototype.transform = function (value, args) {
        switch (args) {
            case 'date':
                return new Date(value).toDateString();
            default:
                return value;
        }
    };
    DataFormatPipePipe = __decorate([
        _angular_core.Pipe({
            name: 'dataFormatPipe'
        })
    ], DataFormatPipePipe);
    return DataFormatPipePipe;
}());

var DataTableModule = (function () {
    function DataTableModule() {
    }
    DataTableModule = __decorate([
        _angular_core.NgModule({
            imports: [
                _angular_common.CommonModule
            ],
            declarations: [DataTableComponent, FilteredTableComponent, PaginatedTableComponent, DataFormatPipePipe],
            exports: [DataTableComponent, FilteredTableComponent, PaginatedTableComponent],
        })
    ], DataTableModule);
    return DataTableModule;
}());

exports.DataTableModule = DataTableModule;
exports.FilterForm = FilterForm;
//# sourceMappingURL=index.js.map
