import { Component, ContentChild, Input } from '@angular/core';
import { FilterForm } from '../../models/filter-form';
import { FilterTable } from '../../models/filter-table';
var FilteredTableComponent = (function () {
    function FilteredTableComponent() {
    }
    FilteredTableComponent.prototype.ngDoCheck = function () {
        this.filterTable.filteredData = this.filteredData;
    };
    Object.defineProperty(FilteredTableComponent.prototype, "filteredData", {
        get: function () {
            return !this.filterForm.filterValue
                ? this.filterTable.allData
                : this.filterTable.allData.filter(this.filterFn(this.filterForm.filterValue));
        },
        enumerable: true,
        configurable: true
    });
    FilteredTableComponent.decorators = [
        { type: Component, args: [{
                    selector: 'filtered-table',
                    templateUrl: './filtered-table.component.html',
                    styleUrls: ['./filtered-table.component.css']
                },] },
    ];
    /** @nocollapse */
    FilteredTableComponent.ctorParameters = function () { return []; };
    FilteredTableComponent.propDecorators = {
        "filterForm": [{ type: ContentChild, args: [FilterForm,] },],
        "filterTable": [{ type: ContentChild, args: [FilterTable,] },],
        "filterFn": [{ type: Input },],
    };
    return FilteredTableComponent;
}());
export { FilteredTableComponent };
//# sourceMappingURL=filtered-table.component.js.map