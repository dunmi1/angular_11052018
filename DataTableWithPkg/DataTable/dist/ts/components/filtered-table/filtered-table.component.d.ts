import { DoCheck } from '@angular/core';
import { FilterForm } from '../../models/filter-form';
import { FilterTable } from '../../models/filter-table';
export declare class FilteredTableComponent implements DoCheck {
    filterForm: FilterForm;
    filterTable: FilterTable;
    filterFn: (filterValue: string) => (value: any, index: number, array: any[]) => boolean;
    ngDoCheck(): void;
    readonly filteredData: any[];
}
