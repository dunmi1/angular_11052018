import { OnInit, AfterContentInit, AfterContentChecked } from '@angular/core';
import { FilterTable } from '../../models/filter-table';
import { PageableTable } from '../../models/pageable-table';
export declare class PaginatedTableComponent extends FilterTable implements OnInit, AfterContentInit, AfterContentChecked {
    data: any[];
    pageLength: number;
    initialPage: number;
    listPropName: string;
    pageableTable: PageableTable;
    readonly allData: any[];
    readonly listPage: any[];
    readonly totalPages: number;
    readonly firstPage: boolean;
    readonly lastPage: boolean;
    ngOnInit(): void;
    ngAfterContentInit(): void;
    ngAfterContentChecked(): void;
    go(pages: number): void;
}
