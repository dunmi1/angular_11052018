export interface DataTableCol {
    header: string;
    field: string;
    format?: string;
}
