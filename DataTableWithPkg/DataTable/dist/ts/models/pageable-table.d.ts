export declare abstract class PageableTable {
    protected _allData: any[];
    protected _pageOfData: any[];
    readonly allData: any[];
    dataPage: any[];
}
