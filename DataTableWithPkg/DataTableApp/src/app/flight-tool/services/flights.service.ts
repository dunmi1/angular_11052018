import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Flight } from '../models/flight';

export class BaseRestService {

  private resourceName: string;

  constructor(resourceName: string) {
    this.resourceName = resourceName;
  }

  protected getCollectionUrl = (queryParams: string[] = null) => {
    let url = `http://localhost:3050/${this.resourceName}`;
    if (queryParams && queryParams.length > 0) {
      url += '?' + queryParams.join('&');
    }
    return url;
  }

}


@Injectable()
export class FlightsService extends BaseRestService {

  constructor(private httpClient: HttpClient) {
    super('flights');
  }

  all = (limit?: number) => {

    const queryParams: string[] = [];

    if (limit) {
      queryParams.push(`_limit=${encodeURIComponent(limit.toString())}`);
    }

    return this.httpClient
      .get<Flight[]>(this.getCollectionUrl(queryParams)).toPromise();
  }

}
