import { Component, OnInit, ViewChild, ApplicationRef } from '@angular/core';

import { Flight } from '../../models/flight';
import { FlightsService } from '../../services/flights.service';
import { FilterForm } from 'data-table';

@Component({
  selector: 'app-flight-home',
  templateUrl: './flight-home.component.html',
  styleUrls: ['./flight-home.component.css']
})
export class FlightHomeComponent implements OnInit {

  public flightTableConfig = {
    cols: [
      { header: 'Date', field: 'flDate', format: 'date' },
      { header: 'Carrier', field: 'carrier' },
      { header: 'Origin', field: 'origin' },
      { header: 'Destination', field: 'dest' }
    ]
  };

  public flights: Flight[] = [];

  @ViewChild(FilterForm)
  public flightFilterForm: FilterForm;

  constructor(
    private flightsService: FlightsService,
    private app: ApplicationRef,
  ) { }

  ngOnInit() {
    this.refreshFlights();
  }

  public refreshFlights() {
    return this.flightsService.all(100)
      .then(flights => {
        if (this.flightFilterForm) {
          this.flightFilterForm.clearForm();
        }
        this.flights = flights;
        // setState
        //this.app.tick();
      });
  }

  public flightFilter = (origin: string) =>
    (flight: Flight) => flight.origin.startsWith(origin)

}
