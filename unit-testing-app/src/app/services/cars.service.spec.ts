import { TestBed, inject } from '@angular/core/testing';

import { Car } from '../models/car';
import { CarsService } from './cars.service';

describe('CarsService', () => {

  let carsService: CarsService = null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CarsService]
    });
  });

  beforeEach(inject([CarsService], (service: CarsService) => {
    carsService = service;
  }));

  it('should be created', () => {
    expect(carsService).toBeTruthy();
  });

  it('get all cars', () => {

    carsService.append(new Car('Ford', 'F-150', 'green', 2017, 12000));
    carsService.append(new Car('Ford', 'F-250', 'red', 2017, 24000));

    const carsCount = carsService.all().length;

    expect(carsCount).toEqual(2);

  });

});
