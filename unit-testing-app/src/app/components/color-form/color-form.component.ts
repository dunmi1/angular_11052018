import { Component, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'color-form',
    templateUrl: './color-form.component.html',
    styleUrls: ['./color-form.component.css'],
})
export class ColorFormComponent {

    public newColorInput = new FormControl('default color');

    @Output()
    public colorSubmitted = new EventEmitter<string>();

    public addNewColor() {
        this.colorSubmitted.emit(this.newColorInput.value);
    }
}
