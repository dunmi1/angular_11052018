import { Car } from './car';

describe('Car', () => {

  it('getInfo', () => {

    const car = new Car('Chevrolet', 'Bolt', 'green', 2015, 34000);

    expect(car.getCarInfo()).toEqual('2015 Chevrolet Bolt');

  });

});
