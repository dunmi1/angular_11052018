import { Injectable } from '@angular/core';

import { DataService } from '../../core/data-service';

import { Car } from '../models/car';

@Injectable({
  providedIn: 'root',
})
export class CarsService extends DataService<Car> {
  constructor() {
    super([
      { id: 1, make: 'Ford', model: 'Fusion', year: 2017, color: 'blue', price: 10000 },
      { id: 2, make: 'Tesla', model: 'S', year: 2016, color: 'red', price: 15000 },
    ]);
  }
}

