import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';

import {
  CarActionTypes, RefreshDoneCarAction, RefreshRequestCarAction,
  AppendRequestCarAction, CarActionUnion
} from './car.actions';
import { Car } from './models/car';
import { RefreshErrorCarAction } from 'src/app/car-tool/car.actions';

@Injectable({
  providedIn: 'root'
})
export class CarEffects {

  constructor(private httpClient: HttpClient, private actions$: Actions<CarActionUnion>) { }

  @Effect()
  refresh$: Observable<RefreshDoneCarAction> = this.actions$.pipe(
    ofType(CarActionTypes.REFRESH_REQUEST),
    mergeMap(() => {

      this.httpClient
        .get<Car[]>('http://localhost:3050/cars')
        .subscribe(cars => {

        }, err => {

        });


    }
      this.httpClient
        .get<Car[]>('http://localhost:3050/cars')
        .pipe(
          map(cars => new RefreshDoneCarAction(cars)),
          catchError(err => new RefreshErrorCarAction(err)),
        )),
  );

  @Effect()
  append$: Observable<RefreshRequestCarAction> = this.actions$.pipe(
    ofType(CarActionTypes.APPEND_REQUEST),
    mergeMap(action =>
      this.httpClient.post(
        'http://localhost:3050/cars',
        (action as AppendRequestCarAction).payload,
      )
        .pipe(map(cars => new RefreshRequestCarAction()))),
  );
}
