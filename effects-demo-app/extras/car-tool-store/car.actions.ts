import { Action } from '@ngrx/store';

import { Car } from './models/car';

export enum CarActionTypes {
  APPEND = '[Car] Add',
  DELETE = '[Car] Delete',
  REPLACE = '[Car] Replace',
  EDIT = '[Car] Edit',
  CANCEL = '[Car] Cancel'
}

export class AppendCarAction implements Action {
  type = CarActionTypes.APPEND;
  constructor(public payload: Car) { }
}

export class DeleteCarAction implements Action {
  type = CarActionTypes.DELETE;
  constructor(public payload: number) { }
}

export class ReplaceCarAction implements Action {
  type = CarActionTypes.REPLACE;
  constructor(public payload: Car) { }
}

export class EditCarAction implements Action {
  type = CarActionTypes.EDIT;
  constructor(public payload: number) { }
}

export class CancelCarAction implements Action {
  type = CarActionTypes.CANCEL;
}

export type CarActionUnion = AppendCarAction | DeleteCarAction |
  ReplaceCarAction | EditCarAction | CancelCarAction;

