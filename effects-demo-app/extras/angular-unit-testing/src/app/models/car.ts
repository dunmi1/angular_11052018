export class Car {

  public id?: number;
  public make: string;
  public model: string;
  public color: string;
  public year: number;
  public price: number;

  constructor(make: string, model: string, color: string, year: number, price: number) {
    this.make = make;
    this.model = model;
    this.color = color;
    this.year = year;
    this.price = price;
  }

  getCarInfo() {
    return `${this.year} ${this.make} ${this.model}`;
  }
}
