import { Component, OnInit } from '@angular/core';

import { Car } from '../../models/car';
import { CarsService } from '../../services/cars';

@Component({
  selector: 'car-tool',
  templateUrl: './car-tool.component.html',
  styleUrls: ['./car-tool.component.css']
})
export class CarToolComponent implements OnInit {

  public cars: Car[] = [];

  public editCarId = -1;

  constructor(private carsSvc: CarsService) { }

  ngOnInit() {
    this.cars = this.carsSvc.all();
  }

  doEditCar(carId: number) {
    this.editCarId = carId;
  }

  doDeleteCar(carId: number) {
    this.cars = this.carsSvc.delete(carId).all();
  }

  doCancelCar() {
    this.editCarId = -1;
  }

  doSaveCar(car: Car) {
    this.cars = this.carsSvc.replace(car).all();
    this.editCarId = -1;
  }

  doAddCar(car: Car) {
    this.cars = this.carsSvc.append(car).all();
  }

}
