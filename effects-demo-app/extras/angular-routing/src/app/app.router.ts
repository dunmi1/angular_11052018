import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'company', loadChildren: './company/company.module#CompanyModule' },
];

export const appRouterModule = RouterModule.forRoot(routes);
