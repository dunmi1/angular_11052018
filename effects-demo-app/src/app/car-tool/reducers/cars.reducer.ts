import {
  CarActionTypes, CarActionUnion,
  DeleteCarAction, ReplaceCarAction, RefreshDoneCarAction,
} from '../car.actions';

import { Car } from '../models/car';

export const carsReducer = (state: Car[] = [], action: CarActionUnion) => {

  switch (action.type) {

    case CarActionTypes.REFRESH_REQUEST:
    case CarActionTypes.APPEND_REQUEST:
    case CarActionTypes.APPEND_DONE:
      return state;
    case CarActionTypes.REFRESH_DONE:
      return (action as RefreshDoneCarAction).payload;
    // Lab Exercise: Implement Delete and Replace with Effects
    // case CarActionTypes.DELETE:
    //   return state.filter(c => c.id !== (action as DeleteCarAction).payload);
    // case CarActionTypes.REPLACE:
    //   const replaceCarAction = action as ReplaceCarAction;
    //   const carIndex = state.findIndex(c => c.id === replaceCarAction.payload.id);
    //   return [ ...state.slice(0, carIndex), replaceCarAction.payload, ...state.slice(carIndex + 1) ];
    default:
      return state;
  }


};
