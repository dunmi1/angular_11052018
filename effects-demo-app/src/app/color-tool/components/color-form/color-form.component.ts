import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

const myRequired = (control: AbstractControl) => {

  if (control.value == null || String(control.value).length === 0) {
    return {
      myRequired: true,
    };
  }

  return null;
};

@Component({
  selector: 'color-form',
  templateUrl: './color-form.component.html',
  styleUrls: ['./color-form.component.css']
})
export class ColorFormComponent implements OnInit {

  @Input()
  public buttonText = 'Submit Form';

  @Output()
  public submitColor = new EventEmitter<string>();

  public colorForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.colorForm = this.fb.group({
      colorInput: [ '', { validators: [ myRequired, Validators.minLength(5) ] } ],
    });
  }

  ngOnInit() {
  }

  doSubmitColor() {
    this.submitColor.emit(this.colorForm.value.colorInput);
    this.colorForm.reset();
  }

}
