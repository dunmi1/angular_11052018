import { Component, OnInit } from '@angular/core';

import { ColorsService } from '../../services/colors.service';


@Component({
  selector: 'color-tool',
  templateUrl: './color-tool.component.html',
  styleUrls: ['./color-tool.component.css'],
})
export class ColorToolComponent implements OnInit {

  public headerText = 'Color Tool';
  public colors = [];

  constructor(private colorsSvc: ColorsService) { }

  ngOnInit() {
    if (this.colorsSvc) {
      this.colors = this.colorsSvc.all().map(c => c.name);
    }
  }

  addColor(name: string) {
    this.colors = this.colorsSvc.append({ name }).all().map(c => c.name);
  }
}
