Exercise 2

1. Create an array of car objects (create only 2 cars). Each object should have the following properties:

id: number (optional)
make: string
model: string
year: number
color: string
price: number

2. Add a table to the car-home component to display the array of car objects. Ensure each column has a header, and be sure to use the tbody and thead tags as appropriate.

3. Ensure the page loads with a table of cars.
