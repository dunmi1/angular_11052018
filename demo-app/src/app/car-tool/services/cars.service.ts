import { Injectable } from '@angular/core';

import { Car, ReadonlyCars, Cars } from '../models/car';

@Injectable({
  providedIn: 'root'
})
export class CarsService {

  private readonlyCars: ReadonlyCars = Object.freeze([
    { id: 1, make: 'Ford', model: 'Fusion Hybrid', year: 2017, color: 'blue', price: 28000 },
    { id: 2, make: 'Chevrolet', model: 'Bolt', year: 2018, color: 'red', price: 40000 },
  ]);

  private set cars(cars: Cars) {
    this.readonlyCars = Object.freeze(cars);
  }

  constructor() { }

  all() {
    return this.readonlyCars;
  }

  one(carId: number) {
    return this.readonlyCars.find(c => c.id === carId);
  }

  append(car: Car) {
    this.cars = this.readonlyCars.concat({
      ...car,
      id: Math.max(...this.readonlyCars.map(c => c.id), 0) + 1,
    });
    return this;
  }

  replace(car: Car) {

    const carIndex = this.readonlyCars.findIndex(c => c.id === car.id);

    if (carIndex === -1) {
      throw new Error('unable to find car');
    }

    const carsCopy  = this.readonlyCars.concat();

    carsCopy[carIndex] = car;
    this.cars = carsCopy;
    return this;
  }

  delete(carId: number) {
    this.cars = this.readonlyCars.filter(c => c.id !== carId);
    return this;
  }
}
