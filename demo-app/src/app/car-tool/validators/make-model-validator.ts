import { FormGroup } from '@angular/forms';

import { MakeModelsArray } from '../models/make-models';

// const makeModelValidator = (makeModels: any, getFG: () => FormGroup) => (c: AbstractControl) => {
//   const formGroup = getFG();
//   if (!formGroup) {
//     return null;
//   }
//   const make = makeModels.find(m => m.name === formGroup.controls.make.value);
//   if (make == null || !make.models.includes(c.value)) {
//     return {
//       makeModelInvalid: {
//         make: c.value,
//         availableModels: make && make.models,
//       }
//     };
//   }
//   return null;
// };

export const makeModelValidator = (makeModels: MakeModelsArray) => (formGroup: FormGroup) => {

  const makeControlValue = formGroup.controls.make.value;
  const modelControlValue = formGroup.controls.model.value;

  const make = makeModels.find(m => m.name === makeControlValue);

  if (make == null || !make.models.includes(modelControlValue)) {
    return {
      makeModelInvalid: {
        make: makeControlValue,
        availableModels: make && make.models,
      }
    };
  }

  return null;
};
