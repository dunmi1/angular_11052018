import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Car } from '../../models/car';
import { makeModelValidator } from '../../validators/make-model-validator';
import { CarCatalogService } from '../../services/car-catalog.service';

@Component({
  selector: 'car-form',
  templateUrl: './car-form.component.html',
  styleUrls: ['./car-form.component.css']
})
export class CarFormComponent implements OnInit {

  @Input()
  buttonText = 'Submit Car';

  @Output()
  submitCar = new EventEmitter<Car>();

  carForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private carCatalog: CarCatalogService,
  ) { }

  ngOnInit() {
    this.carForm = this.fb.group({
      make: [''],
      model: ['', { validators: [
        Validators.required,
        Validators.maxLength(20),
      ]}],
      year: [1900, { validators: [ Validators.min(1895), Validators.max(2019) ]}],
      color: [''],
      price: [0, Validators.min(0) ],
    }, { validator: [ makeModelValidator(this.carCatalog.makeModels) ]});

  }

  doSubmit() {
    if (this.carForm.invalid) {
      window.alert('Form is not valid!');
      return;
    }

    this.submitCar.emit({
      ...this.carForm.value,
    });
  }

}
