import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Color, Colors } from '../models/color';

@Injectable({
  providedIn: 'root',
})
export class ColorsService {

  constructor(private httpClient: HttpClient) { }

  all() {
    return this.httpClient.get<Colors>('http://localhost:4250/colors');
  }

  // one(colorId: number) {
  //   return this.readonlyColors.find(c => c.id === colorId);
  // }

  append(color: Color) {
    return this.httpClient.post<Color>('http://localhost:4250/colors', color);
  }

  // replace(color: Color) {

  //   const colorIndex = this.readonlyColors.findIndex(c => c.id === color.id);

  //   if (colorIndex === -1) {
  //     throw new Error('unable to find color');
  //   }

  //   const colorsCopy  = this.readonlyColors.concat();

  //   colorsCopy[colorIndex] = color;
  //   this.colors = colorsCopy;

  //   return this;
  // }

  // delete(colorId: number) {
  //   this.colors = this.readonlyColors.filter(c => c.id !== colorId);

  //   return this;
  // }
}
