import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

import { Color } from '../../models/color';

@Component({
  selector: 'color-list',
  templateUrl: './color-list.component.html',
  styleUrls: ['./color-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ColorListComponent implements OnInit {

  @Input()
  colors: Color[] = [];

  constructor() { }

  ngOnInit() {
  }

}
