import { Component, OnInit } from '@angular/core';
import { concat } from 'rxjs/operators';

import { Color, Colors } from '../../models/color';
import { ColorsService } from '../../services/colors.service';

@Component({
  selector: 'color-home',
  templateUrl: './color-home.component.html',
  styleUrls: ['./color-home.component.css'],
})
export class ColorHomeComponent implements OnInit {

  colors: Colors = [];

  constructor(private colorsSvc: ColorsService) { }

  ngOnInit() {
    this.colorsSvc.all().subscribe(colors => this.colors = colors);
  }

  addColor(color: Color) {
    // this.colorsSvc
    //   .append(color).pipe(
    //     concat(this.colorsSvc.all())
    //   ).subscribe(colors => this.colors = colors);

      // .subscribe(() => {
      //   this.colorsSvc.all().subscribe(colors => this.colors = colors);
      // });
  }

}
