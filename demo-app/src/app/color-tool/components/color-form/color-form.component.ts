import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';

const myRequiredValidator = (fieldName: string) => (c: AbstractControl) => {

  if (c.value == null || String(c.value).length === 0) {
    return {
      myRequired: {
        message: `${fieldName} is required`,
      }
    };
  }

  return null;

};

import { Color } from '../../models/color';

@Component({
  selector: 'color-form',
  templateUrl: './color-form.component.html',
  styleUrls: ['./color-form.component.css']
})
export class ColorFormComponent implements OnInit {

  colorForm: FormGroup;

  @Output()
  submitColorForm = new EventEmitter<Color>();

  constructor(private fb: FormBuilder) { }

  ngOnInit() {

    this.colorForm = this.fb.group({
      name: ['', { validators: [
        myRequiredValidator('Color name'), Validators.minLength(3) ] }],
      hexCode: ['', { validators: [
        myRequiredValidator('Color hexcode'), Validators.minLength(3) ] }],
      colorInfo: [ { value: { shade: 'test1', color: 'test2' }, disabled: false } ]
    });
  }

  submitColor() {

    this.submitColorForm.emit({
      ...this.colorForm.value,
    });
  }

}
