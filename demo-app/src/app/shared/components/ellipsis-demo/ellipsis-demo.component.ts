import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'ellipsis-demo',
  templateUrl: './ellipsis-demo.component.html',
  styleUrls: ['./ellipsis-demo.component.css']
})
export class EllipsisDemoComponent implements OnInit {

  ellipsisDemoForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {

    this.ellipsisDemoForm = this.fb.group({
      text: '',
      ellipsisLength: 20,
    });

  }

}
