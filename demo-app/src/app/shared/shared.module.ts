import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// models

// module imports

// service imports

// components or pipes import

import { EllipsisDemoComponent } from './components/ellipsis-demo/ellipsis-demo.component';
import { ToolHeaderComponent } from './components/tool-header/tool-header.component';

import { EllipsisPipe } from './pipes/ellipsis.pipe';
import { MyPrependPipe } from './pipes/my-prepend.pipe';
import { MyUppercasePipe } from './pipes/my-uppercase.pipe';

@NgModule({
  declarations: [
    ToolHeaderComponent, MyUppercasePipe, MyPrependPipe,
    EllipsisDemoComponent, EllipsisPipe,
  ],
  imports: [
    CommonModule, ReactiveFormsModule,
  ],
  exports: [
    ToolHeaderComponent, MyUppercasePipe,
    MyPrependPipe, EllipsisDemoComponent,
  ],
})
export class SharedModule { }
