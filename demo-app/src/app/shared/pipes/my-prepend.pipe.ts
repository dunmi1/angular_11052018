import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myPrepend'
})
export class MyPrependPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return args.reduce( (acc, arg) => String(acc) + String(arg), '') + String(value);
  }

}
