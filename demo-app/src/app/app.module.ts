import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { SharedModule } from './shared/shared.module';
import { ColorToolModule } from './color-tool/color-tool.module';
import { CarToolModule } from './car-tool/car-tool.module';
import { CalcToolModule } from './calc-tool/calc-tool.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, SharedModule, ColorToolModule, CarToolModule, CalcToolModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
