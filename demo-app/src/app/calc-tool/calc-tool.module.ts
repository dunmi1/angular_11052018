import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalcHomeComponent } from './components/calc-home/calc-home.component';

@NgModule({
  declarations: [CalcHomeComponent],
  imports: [
    CommonModule
  ],
  exports: [CalcHomeComponent]
})
export class CalcToolModule { }
