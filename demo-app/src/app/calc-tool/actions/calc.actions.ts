import { Action } from '@ngrx/store';

export enum CalcActions {
  Reset = '[Calc] RESET',
  Add = '[Calc] ADD',
  Subtract = '[Calc] SUBTRACT',
  Multiply = '[Calc] MULTIPLY',
  Divide = '[Calc] DIVIDE',
}

export class CalcResetAction implements Action {
  readonly type = CalcActions.Reset;
  constructor() { }
}

export class CalcAddAction implements Action {
  readonly type = CalcActions.Add;
  constructor(public payload: number) {}
}

export class CalcSubtractAction implements Action {
  readonly type = CalcActions.Subtract;
  constructor(public payload: number) {}
}

export class CalcMultiplyAction implements Action {
  readonly type = CalcActions.Multiply;
  constructor(public payload: number) {}
}

export class CalcDivideAction implements Action {
  readonly type = CalcActions.Divide;
  constructor(public payload: number) {}
}

export type CalcActionsUnion = CalcResetAction
  | CalcAddAction | CalcSubtractAction
  | CalcMultiplyAction | CalcDivideAction;
